const Task = require("../Models/Task.js");

module.exports.getTask = (paramsId) => {

	return Task.findById(paramsId).then((result, err) => {
		if(result !== null){
			return result
		} else {
			return "Something went Wrong!"
		}
	}) 
};

module.exports.updateTask = (paramsId) => {
	return Task.findByIdAndUpdate(paramsId, {status : "complete"}).then((result, err) => {
		if (result.status == "complete"){
			return "The Task Status was set to \"Complete\" Already!";
		} else {
			return result, "Status changed to \"Complete\"";
		}
	})
}