// Create the Schema, model and export the file
const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({

	// field: data_type
	name: String,
	// field: { options }
	status: {
		type: String,
		default: "pending"
	}
});

module.exports = mongoose.model("Task", taskSchema);