
const express = require("express");

const taskController = require("../myControllers/myTaskController.js");

const router = express.Router();

router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => {res.send(resultFromController)});
});

router.post("/create", (req, res) => {
	taskController.createTasks(req.body).then(resultFromController => {res.send(resultFromController)});
})

router.get("/:_id", (req, res) => {
	taskController.getTask(req.params).then(resultFromController => {res.send(resultFromController)});
})


module.exports = router; 