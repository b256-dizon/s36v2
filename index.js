// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoutes.js")
const getTask = require("./routes/getTask.js")
const updateTask = require("./routes/updateComplete.js")

// Server setup
const app = express();
const port = 4000;

// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin1234@b256-juaniza.gekbspl.mongodb.net/B256_to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`Were connected to the cloud database`));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}))

app.use("/task", taskRoute)
app.use("/task", getTask)
app.use("/task", updateTask)

// Server Listening
app.listen(port, () => console.log(`Server is running at port ${port}`));

